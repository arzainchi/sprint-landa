-- Adminer 4.7.8 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `p_timeline`;
CREATE TABLE `p_timeline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sprint` varchar(255) DEFAULT NULL,
  `period` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `p_timeline_det`;
CREATE TABLE `p_timeline_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task` varchar(255) DEFAULT NULL,
  `programmer` varchar(255) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `rate` int(11) DEFAULT 0,
  `progress` int(11) DEFAULT 0,
  `nominal` int(11) DEFAULT 0,
  `bonus` int(11) DEFAULT 0,
  `p_timeline_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_p_timeline_det_p_timeline1_idx` (`p_timeline_id`),
  CONSTRAINT `fk_p_timeline_det_p_timeline1` FOREIGN KEY (`p_timeline_id`) REFERENCES `p_timeline` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=736 DEFAULT CHARSET=utf8;


-- 2021-02-10 12:54:03
