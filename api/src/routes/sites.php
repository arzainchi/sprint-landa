<?php

use Model\ApprovalLine;
use Model\Formula;
use Model\HakAkses;
use Model\Karyawan;
use Model\LaporanJadwal;
use Model\OnBoard;
use Model\Perusahaan;
use Model\RiwayatKehadiran;
use Model\Pegawai;
use Service\Db;
use Service\Firebase;
use Service\Landa;

$app->get('/tesnotif', function ($request, $response) {
    $params = $request->getParams();
    $approveLine = new ApprovalLine();
    $approveLine->generateDefault($params['id']);
});

$app->get('/', function ($request, $response) {
    $firebase = new Firebase();
    $formula = new Formula();
    $karyawan = [
        'm_level_jabatan_id' => 2,
        'm_jabatan_id' => 2,
        'm_time_off_id' => 0,
        'm_organisasi_id' => 1,
        'jenkel' => 1,
        'status_nikah' => 1,
        'tipe_karyawan' => 3,
        'karyawan_id' => 1,
    ];
    $listFormula = $formula->getFormula(['id' => 1]);
    $value = $formula->getValue($karyawan, $listFormula['data']);
    echo $value;
    $firebase = new Firebase();
    $firebase->sendNotif('all', 'HAI', 'TES');
});

$app->get('/site/getListPernyataan', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    // print_r($params['filter']);die;
    $data = $db->select('*')
        ->from('m_daftar_pernyataan_petani');


    // if($params['m_tambak_id'])

    $models = $data->findAll();
    $tipe = [];
    foreach ($models as $key => $value) {
        $value->is_setuju = false;
        $tipe[$value->tipe][] = (array) $value;


    }

    return successResponse($response, $tipe);
});

// Ambil set sessions
$app->post('/site/setSessions', function ($request, $response) {
    $params = $request->getParams();
    // if (isset($params['accessToken'])) {
    //     error_reporting(0);
    //     $firebase = new Firebase();
        $landa = new Landa();
        $db = Db::db();
       

        // Ambil data user dari 
        // print_r($params);die;
        if (isset($params['email']) && !empty($params['email'])) {
            $data = $db->select('m_user.*')
            ->from('m_user')
            ->CustomWhere('m_user.email ="'.$params['email'].'" OR m_user.username ="'.$params['email'].'"')
            ->AndWhere('m_user.password','=',sha1($params['password']))
            ->find();
           if(!empty($data)){
            $_SESSION['user']['nama'] = $data->nama;
            $_SESSION['user']['email'] = $data->email;
            $_SESSION['user']['id'] = $data->id;
            $_SESSION['user']['alamat'] = $data->alamat;
            $_SESSION['user']['telepon'] = $data->telepon;
            $_SESSION['user']['ket'] = 'pengguna';
        
            // $_SESSION['user']['is_super_admin'] = $data->is_super_admin;
            // $_SESSION['user']['m_roles_id'] = $data->roles_id;
            // $_SESSION['user']['m_user_id'] = $data->m_user_id;
            $_SESSION['user']['akses'] =json_decode($data->akses);
            return successResponse($response, ['user' => $_SESSION['user']]);
           }
        }

    // }
 

    return unprocessResponse($response, ['User Tidak Ditemukan']);
})->setName('setSession');


// Ambil session user
$app->get('/site/session', function ($request, $response) {
    if (isset($_SESSION['user']['id'])) {
        return successResponse($response, $_SESSION['user']);
    }

    return unprocessResponse($response, ['undefined']);
})->setName('session');


// Hapus semua session
$app->get('/site/logout', function ($request, $response) {
    $firebase = new Firebase();
    $landa = new Landa();

    // if (isset($_SESSION['user']['registrationToken']) && !empty($_SESSION['user']['registrationToken'])) {
    //     if (isset($_SESSION['user']['karyawan_perusahaan']) && [] != $_SESSION['user']['karyawan_perusahaan']) {
    //         $users = json_decode($_SESSION['user']['karyawan_perusahaan']);
    //         foreach ($users as $key => $val) {
    //             $topic = $firebase->unsubscribeTopic($_SESSION['user']['registrationToken'], $landa->idKaryawan($val->id, $_SESSION['user']['client']));
    //         }
    //     }
    //     // $topic = $firebase->unsubscribeTopic($_SESSION['user']['registrationToken'], $landa->idKaryawan($_SESSION['user']['userId'], $_SESSION['user']['client']));
    //     $topic = $firebase->unsubscribeTopic($_SESSION['user']['registrationToken'], 'all_'.$_SESSION['user']['client']);
    // }
    session_destroy();
    return successResponse($response, []);
})->setName('logout');

$app->post('/site/logoutMobile', function ($request, $response) {
    $params = $request->getParams();
    $landa = new Landa();
    $params['karyawan_perusahaan'] = isset($params['karyawan_perusahaan']) ? json_decode($params['karyawan_perusahaan']) : null;

    $firebase = new Firebase();
    if (isset($params['registrationToken']) && !empty($params['registrationToken'])) {
        if (isset($params['karyawan_perusahaan'])) {
            foreach ($params['karyawan_perusahaan'] as $key => $val) {
                $topic = $firebase->unsubscribeTopic($params['registrationToken'], $landa->idKaryawan($val->id, $params['client']));
            }
        }
        // $topic = $firebase->unsubscribeTopic($params['registrationToken'], $landa->idKaryawan($params['userId'], $params['client']));
        $topic = $firebase->unsubscribeTopic($params['registrationToken'], 'all_'.$params['client']);
    }

    return successResponse($response, []);
})->setName('logout');

$app->post('/site/gantiPerusahaanMobile', function ($request, $response) {
    $params = $request->getParams();
    $landa = new Landa();
    $firebase = new Firebase();
    $perusahaan = new Perusahaan();

    // echo json_encode($params);die;
    $perusahaan = new Perusahaan();
    $karyawan_baru = $perusahaan->getKaryawanByPersuhaan($params['userId'], $params['m_perusahaan_id']);

    // if (isset($params['registrationToken']) && !empty($params['registrationToken'])) {
    //     $topic = $firebase->unsubscribeTopic($params['registrationToken'], $landa->idKaryawan($params['userId'], $params['client']));
    //     $topic = $firebase->subscribeTopic($params['registrationToken'], $landa->idKaryawan($karyawan_baru->id, $params['client']));
    // }
    // $_SESSION['user']['userId'] = $karyawan_baru->id;
    // echo json_encode( $karyawan_baru); die;

    return successResponse($response, ['karyawan' => $karyawan_baru]);
})->setName('logout');

// Menampilkan jam server
$app->get('/site/getJam', function ($request, $response) {
    $date = strtotime(date('Y-m-d H:i:s'));

    return successResponse($response, $date);
})->setName('getJam');

// Upload Image
$app->post('/site/uploadImage', function ($request, $response) {
    $namaFile = '';
    $base64Ret = '';
    $res = false;
    if (isset($_FILES['image']['name'])) {
        $imageSampel = $_POST['sampel'];
        $target_dir = 'assets/img/';
        $namaFile = md5(time()).'.'.$_POST['ext'];
        $target_file = $target_dir.$namaFile;
        $check = getimagesize($_FILES['image']['tmp_name']);
        if (false !== $check) {
            if (move_uploaded_file($_FILES['image']['tmp_name'], $target_file)) {
                $ret = true;
            } else {
                $ret = false;
            }
        } else {
            $ret = false;
        }
    } else {
        $ret = false;
    }
    if (true == $ret) {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://us-central1-humanis-2020.cloudfunctions.net/app/face-recognition',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'link_sampel='.$imageSampel.'&base64=https://app.humanis.id/api/assets/img/'.$namaFile,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/x-www-form-urlencoded',
            ],
        ]);

        $_respon = json_decode(curl_exec($curl));
        $respon = (array) $_respon;
        if (false !== strpos($respon['values'], 'person')) {
            $res = true;
            $path = $target_dir.$namaFile;
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64Ret = 'data:image/'.$type.';base64,'.base64_encode($data);
        }
    }

    return successResponse($response, ['status' => $res, 'base64' => $base64Ret]);
})->setName('getJam');

$app->get('/site/getJadwalNow', function ($request, $response) {
    $params = $request->getParams();
    $laporanJadwal = new LaporanJadwal();
    $getDataJadwal = $laporanJadwal->getAll($params);

    foreach ($getDataJadwal['list'] as $key => $val) {
        foreach ($val->jadwal as $keys => $vals) {
            $vals->jam_masuk = substr($vals->jam_masuk, 0, 5);
            $vals->jam_pulang = substr($vals->jam_pulang, 0, 5);
        }
    }

    // pd($getDataJadwal);
    return successResponse($response, ['list' => $getDataJadwal['list'], 'periode' => $getDataJadwal['periode']]);
})->setName('getJadwalNow');


$app->post('/site/setSessions_baru', function ($request, $response) {
    $params = $request->getParams();
    $datauser = new Pegawai();
    // if (isset($params['accessToken'])) {
        error_reporting(0);
        // $firebase = new Firebase();
        $landa = new Landa();
//  print_r($params);die;
        // Ambil data user dari firebase
        if (isset($params['email']) && !empty($params['email'])) {
        //   $datauser = $db->select('*')
        //   ->from('m_user')
        //   ->customWhere('username ='.$params["email"].' OR email = '.$params["email"])
        //   ->andWhere('password','=',sha1($params["password"]))
        //   ->find();
        //   print_r($datauser);
        //   die;

          
        } else {
            $getUser = $firebase->getUserByUid($params['uid']);
        }
        // Ambil client dari firebase untuk setting koneksi database
        if (isset($params['client']) && !empty($params['client']) && isset($_SESSION['user']['client'])) {
            $client = $_SESSION['user']['client'];
        } else {
            $client = $getUser['data']['client'];
        }
        $getClient = $firebase->getClientByUid($client);

        // set database ke session
        if (isset($getClient['data']['db'])) {
            $_SESSION['user']['safeEmailId'] = $getUser['data']['client'];
            $_SESSION['user']['safeEmail'] = [
                'safeEmail1' => $getClient['data']['db']['DB_HOST'],
                'safeEmail2' => $getClient['data']['db']['DB_NAME'],
                'safeEmail3' => $getClient['data']['db']['DB_PASS'],
                'safeEmail4' => $getClient['data']['db']['DB_USER'],
            ];
        }

        $_SESSION['user']['client'] = $client;

        try {
            // Ambil karyawan berdasarkan UID
            $karyawan = new Karyawan();
            $getKaryawan = $karyawan->getAllKaryawan(['uid' => $getUser['data']['uid']]);

            // echo json_encode($getKaryawan);die;
            $m_roles_id = isset($getKaryawan['data'][0]->m_roles_id) ? $getKaryawan['data'][0]->m_roles_id : 1;

            if (-1 == $m_roles_id && isset($params['sumber']) && 1 == $params['sumber']) {
                return unprocessResponse($response, ['Login ditolak !, anda tidak mempunyai akses']);
            }

            // Ambil hak akses
            $akses = new HakAkses();
            $getAkses = $akses->getAll(['id' => $m_roles_id], 1, 0);
            $hakAkses = isset($getAkses['data'][0]) ? (array) $getAkses['data'][0] : [];
            $aksesPerusahaan = isset($hakAkses['akses_perusahaan']) ? $hakAkses['akses_perusahaan'] : [];
        } catch (PDOException $e) {
            return unprocessResponse($response, $e->getMessage());
        }

        // Jika tipe super admin Ambil list semua perusahaan
        if (isset($getUser['data']['tipe']) && ('admin' == $getUser['data']['tipe'] || 'super admin' == $getUser['data']['tipe'])) {
            $user = $getUser;
            $perusahaan = new Perusahaan();
            $allPerusahaan = $perusahaan->getAll([], '', '', 'id asc');
            foreach ($allPerusahaan['data'] as $key => $value) {
                $getPerusahaan['data'][] = [
                    'id' => $value->id,
                    'nama' => $value->nama,
                ];

                break;
            }

            // ambil hak akses super admin
            $akses = new HakAkses();
            $getAkses = $akses->getAll(['id' => 1], 1, 0);
            $hakAkses = isset($getAkses['data'][0]) ? (array) $getAkses['data'][0] : [];

        // Jika tipe karyawan ambil list perusahaan berdasarkan akses / jabatan
        } elseif (count($getKaryawan['data']) >= 1) {
            // set data karyawan ke variable user
            $user['data']['userId'] = $getKaryawan['data'][0]->id;
            $user['data']['nik'] = $getKaryawan['data'][0]->nik;
            $user['data']['statusTxt'] = '1' == $getKaryawan['data'][0]->tipe ? 'Kontrak' : 'Tetap';
            $user['data']['jabatanTxt'] = $getKaryawan['data'][0]->nama_jabatan;
            $user['data']['nama'] = $getKaryawan['data'][0]->nama;
            $user['data']['email'] = $getKaryawan['data'][0]->email;
            $user['data']['uid'] = $getKaryawan['data'][0]->uid;
            $user['data']['client'] = $getUser['data']['client'];
            $user['data']['data_lengkap'] = $getKaryawan['data'][0];
            $user['data']['karyawan_perusahaan'] = json_encode($getKaryawan['data']);
            // echo json_encode($getKaryawan['data'][0]->foto);die;
            $user['data']['foto'] = !empty($getKaryawan['data'][0]->foto) ? config('SITE_IMG').'karyawan/'.$getKaryawan['data'][0]->foto : '';
            //Get Foto ketika edit dari mobile(beda folder)
            // if (!file_exists($user['data']['foto'])) {
            //     $user['data']['foto'] =  !empty($getKaryawan['data'][0]->foto) ?  config('SITE_IMG').'mobile/'.$getKaryawan['data'][0]->foto : '';
            // }
            // Hak akses karyawan ambil list perusahaan berdasarkan jabatan
            if (-1 == $m_roles_id) {
                foreach ($getKaryawan['data'] as $key => $value) {
                    $getPerusahaan['data'][] = [
                        'id' => $value->m_perusahaan_id,
                        'nama' => $value->m_perusahaan_nama,
                    ];

                    break;
                }
                // Hak akses non karyawan
            } else {
                $perusahaan = new Perusahaan();
                $list = $perusahaan->getByIdArr($aksesPerusahaan);
                // echo json_encode($list);die;
                // echo json_encode($getKaryawan['data'][0]->m_perusahaan_id);die;

                foreach ($list as $key => $value) {
                    if (!isset($params['sumber']) || 2 == $params['sumber']) {
                        if ($getKaryawan['data'][0]->m_perusahaan_id == $value->id) {
                            $getPerusahaan['data'][] = [
                                'id' => $value->id,
                                'nama' => $value->nama,
                            ];

                            break;
                        }
                    } else {
                        $getPerusahaan['data'][] = [
                            'id' => $value->id,
                            'nama' => $value->nama,
                        ];

                        break;
                    }
                }

                if (!isset($params['sumber']) || 2 == $params['sumber']) {
                    $perusahaanData = $perusahaan->getById($getKaryawan['data'][0]->m_perusahaan_id);
                    $getPerusahaan['data'][] = [
                        'id' => $perusahaanData->id,
                        'nama' => $perusahaanData->nama,
                    ];
                }
            }
            // echo json_encode($getPerusahaan['data']);die;
        }

        // set perusahaan aktif ke session
        if (!empty($getPerusahaan['data'])) {
            foreach ($getPerusahaan['data'] as $key => $value) {
                $_SESSION['user']['m_perusahaan'] = [
                    'id' => $value['id'],
                    'nama' => $value['nama'],
                ];

                break;
            }
        } else {
            $_SESSION['user']['m_perusahaan'] = [
                'id' => 0,
                'nama' => 'Unknown Company',
            ];
        }

        // jika user ditemukan, simpan ke session, jika tidak tampilkan login gagal
        if (isset($user['data']['uid']) && !empty($user['data']['uid'])) {
            // set subscribe untuk notifikasi
            // $perusahaan = isset($_SESSION['user']['m_perusahaan']['id']) ? $_SESSION['user']['m_perusahaan']['id'] : 0;

            // set PHP Session
            $_SESSION['user']['userId'] = isset($user['data']['userId']) ? $user['data']['userId'] : 0;
            $_SESSION['user']['client'] = $user['data']['client'];
            $_SESSION['user']['nik'] = isset($user['data']['nik']) ? $user['data']['nik'] : '-';
            $_SESSION['user']['statusTxt'] = isset($user['data']['statusTxt']) ? $user['data']['statusTxt'] : '-';
            $_SESSION['user']['jabatanTxt'] = isset($user['data']['jabatanTxt']) ? $user['data']['jabatanTxt'] : '-';
            $_SESSION['user']['uid'] = $user['data']['uid'];
            // $_SESSION['user']['accessToken'] = $params['accessToken'];
            $_SESSION['user']['nama'] = $user['data']['nama'];
            $_SESSION['user']['email'] = $user['data']['email'];
            $_SESSION['user']['tipe'] = isset($user['data']['tipe']) ? $user['data']['tipe'] : 'karyawan';
            $_SESSION['user']['foto'] = isset($user['data']['foto']) && !empty($user['data']['foto']) ? $user['data']['foto'] : img_url().'default.png';
            $_SESSION['user']['id'] = $_SESSION['user']['userId'];
            $_SESSION['user']['data_lengkap'] = isset($user['data']['data_lengkap']) ? $user['data']['data_lengkap'] : '-';
            $_SESSION['user']['karyawan_perusahaan'] = isset($user['data']['karyawan_perusahaan']) ? $user['data']['karyawan_perusahaan'] : null;
            $_SESSION['user']['registrationToken'] = isset($params['registrationToken']) ? $params['registrationToken'] : null;
            // $firebase->subscribeTopic($params['accessToken'], $params['uid']);

            //Subscribe karyawan di semua perusahaan
            if (isset($params['registrationToken']) && !empty($params['registrationToken'])) {
                if (isset($getKaryawan['data']) && count($getKaryawan['data']) >= 0) {
                    foreach ($getKaryawan['data'] as $key => $val) {
                        $topic = $firebase->subscribeTopic($params['registrationToken'], $landa->idKaryawan($val->id, $_SESSION['user']['client']));
                    }
                }
                $topic = $firebase->subscribeTopic($params['registrationToken'], 'all_'.$user['data']['client']);
            }

            $json = json_encode($_SESSION['user']);
            $base64 = base64_encode($json);

            // Masukkan hak akses ke session
            $_SESSION['user']['akses'] = isset($hakAkses['akses']) ? $hakAkses['akses'] : [];
           
            return successResponse($response, ['user' => $_SESSION['user'], 'base64' => $base64]);
        }

        return unprocessResponse($response, ['User belum terdaftar pada sistem']);
    // }

    return unprocessResponse($response, ['undefined']);
})->setName('setSession');

