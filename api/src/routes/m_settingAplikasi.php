<?php
use Service\Db;
use Service\Firebase;
use Service\Landa;


function validasi($data, $custom = array())
{
    $validasi = array(
        "nama_aplikasi" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}


$app->get('/m_settingAplikasi/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $data = $db->select('*')
               ->from('m_setting');
            $models = $data->find();
            
             $model = !empty($models) ? $models : [];
        //  echo json_encode($model);die;
    return successResponse($response,$model);
});

$app->post("/m_settingAplikasi/save", function ($request, $response) {
    $data   = $request->getParams();
    $db = Db::db();
    $landa = new Landa();  
     
    $validasi  = validasi($data);
    if ($validasi === true) {
        try {       
            if (isset($data['logo_login']['base64']) && !empty( $data['logo_login']['base64'])) {
                $path = 'assets/img/setting_aplikasi/';
                $uploadFile = $landa->base64ToImage($path,  $data['logo_login']['base64']);   

                if ($uploadFile) {
                     $data['logo_login'] = $uploadFile['data'];
                  
                } else {
                    return unprocessResponse($response, [$uploadFile['error']]);
                }
               
            }else{
                unset($data['logo_login']);
            }
            if (isset($data['logo_menu']['base64']) && !empty( $data['logo_menu']['base64'])) {
                $path = 'assets/img/setting_aplikasi/';
                $uploadFile = $landa->base64ToImage($path,  $data['logo_menu']['base64']);   

                if ($uploadFile) {
                     $data['logo_menu'] = $uploadFile['data'];
                  
                } else {
                    return unprocessResponse($response, [$uploadFile['error']]);
                }
               
            }else{
                unset($data['logo_menu']);
            }
            if (isset($data['logo_laporan']['base64']) && !empty( $data['logo_laporan']['base64'])) {
                $path = 'assets/img/setting_aplikasi/';
                $uploadFile = $landa->base64ToImage($path,  $data['logo_laporan']['base64']);   

                if ($uploadFile) {
                     $data['logo_laporan'] = $uploadFile['data'];
                  
                } else {
                    return unprocessResponse($response, [$uploadFile['error']]);
                }
               
            } else{
                unset($data['logo_laporan']);
            }
            // echo json_encode($data);die;
            if (isset($data["id"])) {
              
                $model = $db->update("m_setting", $data, ["id" => $data["id"]]);
            } else {
               
                $model = $db->insert("m_setting", $data);
            }
            return successResponse($response,  $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});



$app->post("/m_settingAplikasi/delete", function ($request, $response) {
    $data     = $request->getParams();
    $db = Db::db();
    $model = $db->delete("m_setting", ['id' => $data['id']]);
    if (isset($model)) {
        return successResponse($response, [$model]);
    }

    return unprocessResponse($response, ['terjadi masalah pada server']);
});








?>