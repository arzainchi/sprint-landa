<?php
use Service\Db;
use Service\Firebase;
use Service\Landa;


function validasi($data, $custom = array())
{
    $validasi = array(
        "nama" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
$app->get('/m_hakakses/index', function ($request, $response) {
    $params = $request->getParams();
    $db = Db::db();
    $data = $db->select('*')
               ->from('m_roles')
               ->where('m_roles.is_deleted', '=', 0);
               

               if (isset($params["filter"])) {
                $filter = (array) json_decode($params["filter"]);
                foreach ($filter as $key => $val) {
                    $data->where($key, "LIKE", $val);
                }
            }
            if (isset($params["limit"]) && !empty($params["limit"])) {
                $data->limit($params["limit"]);
            }
            if (isset($params["offset"]) && !empty($params["offset"])) {
                $data->offset($params["offset"]);
            }

            $models = $data->findAll();
            $totalItem = $data->count();
            foreach ($models as $val) {
                $val->akses = json_decode($val->akses);
            }
    return successResponse($response, [
        'list' => $models,
        'totalItems'=>$totalItem
       
    ]);
});

$app->post("/m_hakakses/save", function ($request, $response) {
    $data     = $request->getParams();
    $db = Db::db();
    $validasi = validasi($data);
    // print_r($data);die;
    if ($validasi === true) {
        try {
            $data["akses"] = json_encode($data["akses"]);
            if (isset($data["id"])) {
                $model = $db->update("m_roles", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_roles", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post("/m_hakakses/delete", function ($request, $response) {
    $data     = $request->getParams();
    $db = Db::db();
    $model = $db->update("m_roles", ['is_deleted' => 1], ['id' => $data['id']]);
    if (isset($model)) {
        return successResponse($response, [$model]);
    }

    return unprocessResponse($response, ['terjadi masalah pada server']);
});




?>