*Done-> ALTER TABLE `m_bahan_penunjang` CHANGE `is_deleted` `is_deleted` INT(11) NULL DEFAULT '0';
*Done-> ALTER TABLE `m_size` CHANGE `m_size_kategori_id` `m_size_kategori_id` INT(11) NULL DEFAULT NULL;
*Done->ALTER TABLE `m_harga` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, add PRIMARY KEY (`id`);
*Done->ALTER TABLE `m_tambak_detail_bahan` CHANGE `masa aplikasi` `masa_aplikasi` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'persiapan lahan, persiapan budidaya, masa budidaya, panen';
*Done->ALTER TABLE `m_pembenihan` CHANGE `is_deleted` `is_deleted` INT(11) NULL DEFAULT '0';

*Done->CREATE TABLE `db_atina`.`m_kode_form` ( `id` INT NULL AUTO_INCREMENT , `nama` VARCHAR(45) NULL DEFAULT NULL , `kode` VARCHAR(45) NULL DEFAULT NULL , `is_deleted` INT NULL DEFAULT '0' , `created_at` INT NULL DEFAULT NULL , `created_by` INT NULL DEFAULT NULL , `modified_at` INT NULL DEFAULT NULL , `modified_by` INT NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
order pada t_penerimaan tidak jalan mohon di ganti


*Done->ALTER TABLE `m_spesies`
ADD `nama_latin` varchar(255) COLLATE 'utf8mb4_general_ci' NULL AFTER `keterangan`;

*Done->ALTER TABLE `m_jadwal_kerja`
CHANGE `is_draft` `is_draft` int(11) NULL DEFAULT '0' AFTER `tanggal`,
CHANGE `is_deleted` `is_deleted` int(11) NULL DEFAULT '0' AFTER `is_draft`;

*Done->ALTER TABLE `m_jadwal_kerja_det`
CHANGE `jam` `jam_mulai` int(11) NULL AFTER `tanggal`,
ADD `jam_selesai` int(11) NULL;


*done ALTER TABLE `m_harga`
CHANGE `m_supplier_id` `m_supplier_id` text NULL AFTER `jenis`;

ALTER TABLE `m_supplier`
CHANGE `telepon` `telepon` text COLLATE 'utf8_general_ci' NULL AFTER `berdiri_tahun`,
CHANGE `bank` `bank` text COLLATE 'utf8_general_ci' NULL AFTER `npwp`;

CREATE TABLE `t_penerimaan_hasil_panen` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `t_penerimaan_id` int NULL,
  `grade` varchar(45) NOT NULL,
  `qty` double NOT NULL
);

 done ->ALTER TABLE `m_harga`
ADD `m_grade_id` int(11) NULL AFTER `m_size_id`;

done ->ALTER TABLE `m_jadwal_kerja_detail`
ADD `is_inspeksi` int(1) NULL COMMENT '1 ya 0 tidak';

CREATE TABLE `t_pembelian_insentif` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nama_pengirim` varchar(45) NULL,
  `nama_tambak` varchar(45) NULL,
  `kode_tambak` varchar(45) NULL,
  `spesies` varchar(45) NULL,
  `besaran_pajak` varchar(45) NULL,
  `status` varchar(45) NULL,
  `tanggal` date NULL,
  `asal_rm` varchar(45) NULL,
  `metode_pengiriman` varchar(45) NULL,
  `biaya_perahu` int(11) NULL,
  `total_asalrm` int(11) NULL,
  `sub_total` int(11) NULL,
  `total_biaya_perahu` int(11) NULL,
  `qty_perahu` int(11) NULL,
  `potongan_pajak` int(11) NULL,
  `qty_pajak` int(11) NULL,
  `pajak_ditanggung_atina` int(11) NULL,
  `qty_pajak_atina` int(11) NULL,
  `grand_total` int(11) NULL
);



CREATE TABLE `t_pembelian_insentif_det` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `t_pembelian_insentif_id` int NULL,
  `kategori_size` varchar(45) NULL,
  `size` int NULL,
  `kuantiti` int NULL,
  `harga` int NULL,
  `total` int NULL,
  `total_persize` int NULL
);

CREATE TABLE `t_pembelian_insentif_kategori` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `t_pembelian_insentif_id` int(11) NULL,
  `kategori` varchar(45) NULL,
  `kuantiti` int(11) NULL,
  `total` int(11) NULL
);

'ALTER TABLE `t_pembelian_insentif`
ADD `kota` int(11) NULL AFTER `m_tambak_id`;
(0.017 s)'