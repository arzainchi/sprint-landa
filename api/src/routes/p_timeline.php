<?php
use Service\Db;
use Service\Firebase;
use Service\Landa;

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(                     
        "name"  => "required",    
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}
/**
 * Ambil semuakategori
 */
$app->get("/p_timeline/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = Db::db();
    $db->select("*")
        ->from("p_timeline");    
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    // $models = [];
    $models  = $db->orderBy('id DESC')->findAll();
    $totalItem = $db->count();
    foreach ($models as $project) {
        $timeline = getTimelineDetail(DB::db(), $project->id);
        $project->timeline = $timeline;

        $date = $db->select('due_date')
            ->from('p_timeline_det')
            ->where('p_timeline_id', '=', $project->id)
            ->groupBy('due_date')
            ->findAll();
        $vals = [];
        foreach ($date as $val) {
            $vals[] = $val->due_date;
        }
        $project->date = $vals;
    }
    // $models    = $db->findAll();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Savekategori
 */
$app->post("/p_timeline/save", function ($request, $response) {
    $data     = $request->getParams();
    $db       = Db::db();

    try {
        if (isset($data['dataProject']["id"])) {
            // update table p_timeline
            $update = [
                'name'      => $data['dataProject']['name'],
                'period'    => $data['dataProject']['period'],
                'sprint'    => $data['dataProject']['sprint']
            ];
            $model = $db->update("p_timeline", $update, ["id" => $data['dataProject']["id"]]);

            // update data p_timeline_det
            $db->delete('p_timeline_det', ['p_timeline_id' => $data['dataProject']['id']]);
            insertTimeline(Db::db(), $data['dataTimeline'], $data['dataProject']['id']);
        } else {
            $insert = [
                'name'      => $data['dataProject']['name'],
                'period'    => $data['dataProject']['period'],
                'sprint'    => $data['dataProject']['sprint'],
            ];
            $model = $db->insert("p_timeline", $insert);

            // insert timeline_det
            $project_id = $db->query("SELECT id from p_timeline ORDER BY id DESC LIMIT 1")->fetchColumn();
            insertTimeline(DB::db(), $data["dataTimeline"], $project_id);
        }
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, ["terjadi masalah pada server"]);
});
/**
 * Hapuskategori
 */
$app->post("/p_timeline/hapus", function ($request, $response) {
    $data     = $request->getParams();
    $db       = Db::db();
    try {
        $db->delete('p_timeline_det', ['p_timeline_id' => $data['id']]);
        $model = $db->delete("p_timeline", ["id" => $data["id"]]);        
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, ['terjadi masalah pada server']);
});

function insertTimeline($db, $dataTimeline, $project_id) {
    foreach ($dataTimeline as $programmer) {
        $namaProgrammer = $programmer['programmer'];
        $bonus          = $programmer['bonus'];
        foreach ($programmer['timeline'] as $data) {
            $due_date = $data["date"];
            $rate = $data["rate"];
            $nominal = $data["nominal"];
            foreach ($data["tasks"] as $task) {
                $insert = [
                    'programmer'    => $namaProgrammer,
                    'task'          => $task['name'],
                    'progress'      => $task['progress'],
                    'due_date'      => $due_date,
                    'rate'          => $rate,
                    'nominal'       => $nominal,
                    'bonus'         => $bonus,
                    'p_timeline_id' => $project_id
                ];
                $db->insert('p_timeline_det', $insert);
            }
        }
    }
}

function getTimelineDetail($db, $project_id) {
    $rawData = $db->select('*')->from('p_timeline_det')->where('p_timeline_id', '=', $project_id)->findAll();
    $returnData = [];
    $index = 0;
    $indexTimeline = 0;
    foreach ($rawData as $data) {
        if (count($returnData) == 0) {
            $row = [
                'programmer' => $data->programmer,
                'jumlah'     => 1,
                'bonus'      => $data->bonus,
                'timeline'   => [
                    [
                        'date' => $data->due_date,
                        'jumlah'    => 1,
                        'tasks'     => [
                            [
                                'name'      => $data->task,
                                'progress'  => $data->progress,
                            ],
                        ],
                        'avgProgress'   => 0,
                        'rate'          => $data->rate,
                        'nominal'       => $data->nominal,
                    ],
                ],
            ];
            $returnData[] = $row;
            $index++;
        } else {
            if ($returnData[($index - 1)]['programmer'] == $data->programmer) { // jika programmer sama
                if ($returnData[($index - 1)]['timeline'][$indexTimeline]['date'] == $data->due_date) { // jika tanggal sama
                    $row = [
                        'name'      => $data->task,
                        'progress'  => $data->progress,
                    ];
                    $returnData[($index - 1)]['timeline'][$indexTimeline]['jumlah'] += 1;
                    $returnData[($index - 1)]['timeline'][$indexTimeline]['tasks'][] = $row;
                } else { // beda tanggal
                    $row = [
                        'date' => $data->due_date,
                        'jumlah'    => 1,
                        'tasks'     => [
                            [
                                'name'      => $data->task,
                                'progress'  => $data->progress,
                            ],
                        ],
                        'avgProgress'   => 0,
                        'rate'          => $data->rate,
                        'nominal'         => $data->nominal,
                    ];
                    $returnData[($index - 1)]['timeline'][] = $row;
                    $indexTimeline++;
                }
                $returnData[($index - 1)]['jumlah'] += 1;
            } else { // jika beda programmer
                $indexTimeline = 0;
                $row = [
                    'programmer' => $data->programmer,
                    'jumlah'     => 1,
                    'bonus'      => $data->bonus,
                    'timeline'   => [
                        [
                            'date' => $data->due_date,
                            'jumlah'    => 1,
                            'tasks'     => [
                                [
                                    'name'      => $data->task,
                                    'progress'  => $data->progress,
                                ],
                            ],
                            'avgProgress'   => 0,
                            'rate'          => $data->rate,
                            'nominal'       => $data->nominal,
                        ],
                    ],
                ];
                $returnData[] = $row;
                $index++;
            }
        }
    }
    return $returnData;
}

function getMonthDate($date) {
    $timeline = 0;
    $month = [];
    $datee = [];
    $listMonth = [
      'Januari', 'Feburari', 'Maret', 'April', 'Mei', 'Juni',
      'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
    ];

    $index = 0;
    // var_dump($date);die;
    foreach ($date as $tgl) {
        $datee[] = date('d', strtotime($tgl));
        if (count($month) == 0) {
            $newValue = [
                'name' => $listMonth[((int) date('m', strtotime($tgl)) - 1)],
                'colspan' => 1
            ];
            $month[] = $newValue;
            $timeline++;
            $index++;
        } else {
            if (in_array($listMonth[((int) date('m', strtotime($tgl)) - 1)], array_column($month, 'name'))) {
                $month[($index - 1)]['colspan'] += 1;
                $timeline++;
            } else {
                $newValue = [
                    'name' => $listMonth[((int) date('m', strtotime($tgl)) - 1)],
                    'colspan' => 1
                ];
                $month[] = $newValue;
                $timeline++;
                $index++;
            }
        }
    }
    return [
        'dates' => $datee,
        'months'=> $month,
        'timeline' => $timeline
    ];
}

$app->get('/p_timeline/cetak', function($request, $response) {
    $param = $request->getParams();

    // get data project
    $project = Db::db()->select('*')
        ->from('p_timeline')
        ->where('id', '=', $param['id'])
        ->find();

    // get data timeline
    $timeline = getTimelineDetail(DB::db(), $param['id']);
    // print_r($timeline);die;

    // get header month and date
    $date = DB::db()->select('due_date')
        ->from('p_timeline_det')
        ->where('p_timeline_id', '=', $param['id'])
        ->groupBy('due_date')
        ->findAll();
    $vals = [];
    foreach ($date as $val) {
        $vals[] = $val->due_date;
    }
    $date = getMonthDate($vals);
    // var_dump($date);die;

    // PDF
    $pdf = new TCPDF('L', 'mm', 'A4');

    // remove header and footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    // add Page
    $pdf->AddPage();
    $pdf->SetAutoPageBreak(true, 0);

    // add content
    $pdf->SetFont('Helvetica', '', 14);
    $pdf->Cell('280', '10', 'REKAP SPINT '. $project->sprint .', Project '. $project->name, 0, 1, 'C');

    $pdf->SetFont('Helvetica', '', 8);
    $pdf->Cell('280', '8', 'Periode '. $project->period, 0, 1, 'C');
    $pdf->ln();

    $pdf->SetFont('Helvetica', '', 10);
    $pdf->SetFillColor(248, 249, 250);
    $pdf->SetLineStyle(array('width' => 0.5, 'color' => array(0, 0, 0)));
    $pdf->SetTextColor(0, 0, 0);
    $pdf->MultiCell(30, 15, "Programmer", 1, 'C', 1, 0, '', '', true, 0, false, true, 15, 'M');
    $pdf->MultiCell(75, 15, "Task", 1, 'C', 1, 0, '', '', true, 0, false, true, 15, 'M');
    $xTanggal = $pdf->GetX();
    $pdf->MultiCell(75, 5, "Timeline", 1, 'C', 1, 0, '', '', true, 0, false, true, 5, 'M');
    $y = $pdf->GetY() + 5;
    $pdf->MultiCell(30, 15, "Rate", 1, 'C', 1, 0, '', '', true, 0, false, true, 15, 'M');
    $pdf->MultiCell(10, 15, "Test", 1, 'C', 1, 0, '', '', true, 0, false, true, 15, 'M');
    $pdf->MultiCell(30, 15, "Nominal", 1, 'C', 1, 0, '', '', true, 0, false, true, 15, 'M');
    $pdf->MultiCell(30, 15, "Bonus", 1, 'C', 1, 1, '', '', true, 0, false, true, 15, 'M');
    
    // bulan -> width : 120 / jumlah bulan
    $index = 0;
    $x = '';
    foreach ($date['months'] as $month) {
        if ($index == 0) {
            $pdf->MultiCell((75 / count($date['months'])), 5, $month['name'], 1, 'C', 1, 0, $xTanggal, $y, true, 0, false, true, 15, 'T');
            $x = $pdf->getX();
        } else {
            $pdf->MultiCell((75 / count($date['months'])), 5, $month['name'], 1, 'C', 1, 0, $x, $y, true, 0, false, true, 15, 'T');
            $x = $pdf->GetX();
        }
        $index++;
    }
    $y = $pdf->GetY() + 5;

    // tanggal -> width : 75 / jumlah tanggal;
    $pdf->SetFont('Helvetica', '', 5);
    $x = '';
    $index = 0;
    foreach ($date['dates'] as $tgl) {
        if ( $index == 0 ) {
            $pdf->MultiCell((75 / count($date['dates'])), 5, $tgl, 1, 'C', 1, 0, $xTanggal, $y, true, 0, false, true, 5, 'M');
            $x = $pdf->getX();
        } else {
            $pdf->MultiCell((75 / count($date['dates'])), 5, $tgl, 1, 'C', 1, 0, $x, $y, true, 0, false, true, 5, 'M');
            $x = $pdf->GetX();
        }
        $index++;
    }

    // Data timeline
    $pdf->Ln();
    $pdf->SetFont('Helvetica', '', 8);
    $differentProgrammer = true;
    $differentDate = true;
    $halaman = 1;
    $jumlahTaskProgrammer = 0;
    $kuotaDataPerhalaman = 14;
    $xTask = '';
    $yTask = '';
    $xRate = '';
    $yRate = '';
    $xNominal = '';
    $yProgress = '';
    $utangRowspan = 0;
    $countData = 0;
    $pdf->SetFillColor(100, 0, 0, 0);
    // $indexTimeline = '';
    // print_r($timeline);die;
    foreach ($timeline as $item) {
        $differentProgrammer = true;
        $jumlahTaskProgrammer = $item['jumlah'];
        foreach ($item['timeline'] as $row) {    
            $differentDate = true;
            foreach ($row['tasks'] as $val) {
                $countData++;
                if ($differentProgrammer) {

                    // NAMA PROGRAMMER
                    if ($jumlahTaskProgrammer < $kuotaDataPerhalaman) {
                        $pdf->MultiCell(30, 10 * $jumlahTaskProgrammer, $item['programmer'], 1, 'L', 0, 0, 10, '', true, 0, false, true, 10 * $jumlahTaskProgrammer, 'M');
                    } else {
                        $pdf->MultiCell(30, 10 * $kuotaDataPerhalaman, $item['programmer'], 1, 'L', 0, 0, 10, '', true, 0, false, true, 10 * $kuotaDataPerhalaman, 'M');
                    }
                    // $pdf->MultiCell(30, 10 * $kuotaDataPerhalaman, $item['programmer'], 1, 'L', 0, 0, 10, '', true, 0, false, true, 10 * $kuotaDataPerhalaman, 'M');
                    $xTask = $pdf->getX();
                    $yTask = $pdf->getY() + 10;

                    // task
                    $pdf->MultiCell(75, 10, $val['name'], 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 'M');
                    $jumlahTaskProgrammer--;
                    $kuotaDataPerhalaman--;

                    // timeline
                    foreach ($vals as $date2) {
                        if ($date2 == $row['date']) {
                            $pdf->MultiCell((75 / count($date['dates'])), 10, '', 1, 'C', 1, 0, '', '', true, 0, false, true, 10, 'M');
                        } else {
                            $pdf->MultiCell((75 / count($date['dates'])), 10, '', 1, 'C', 0, 0, '', '', true, 0, false, true, 10, 'M');
                        }
                    }

                    // RATE                    
                    if ($row['jumlah'] > ($kuotaDataPerhalaman+1)) {
                        $pdf->MultiCell(30, (10 * ($kuotaDataPerhalaman+1)), 'Rp '. number_format($row['rate'], 2, ',', '.'), 1, 'C', 0, 0, '', '', true, 0, false, true, (10 * ($kuotaDataPerhalaman+1)), 'M');
                        $xRate = $pdf->getX() - 30;
                        $yRate = $pdf->GetY() + (10 * $row['jumlah']);
                        $utangRowspan = $row['jumlah'] - ($kuotaDataPerhalaman+1);
                    } else {
                        $pdf->MultiCell(30, (10 * $row['jumlah']), 'Rp '. number_format($row['rate'], 2, ',', '.'), 1, 'C', 0, 0, '', '', true, 0, false, true, (10 * $row['jumlah']), 'M');
                        $xRate = $pdf->getX() - 30;
                        $yRate = $pdf->GetY() + (10 * $row['jumlah']);
                    }

                    // PROGRRESS
                    $pdf->MultiCell(10, 10, $val['progress'] . '%', 1, 'C', 0, 0, '', '', true, 0, false, true, 10, 'M');
                    $yProgress = $pdf->getY() + 10;

                    // NOMINAL
                    if ($row['jumlah'] > ($kuotaDataPerhalaman+1)) {
                        $pdf->MultiCell(30, (10 * ($kuotaDataPerhalaman+1)), 'Rp '. number_format($row['nominal'], 2, ',', '.'), 1, 'C', 0, 0, '', '', true, 0, false, true, 10 * ($kuotaDataPerhalaman+1), 'M');
                        $xNominal = $pdf->GetX();
                    } else {
                        $pdf->MultiCell(30, (10 * $row['jumlah']), 'Rp '. number_format($row['nominal'], 2, ',', '.'), 1, 'C', 0, 0, '', '', true, 0, false, true, 10 * $row['jumlah'], 'M');
                        $xNominal = $pdf->GetX();
                    }

                    // BONUS    
                    if (($jumlahTaskProgrammer+1) < ($kuotaDataPerhalaman+1)) {                
                        $pdf->MultiCell(30, 10 * ($jumlahTaskProgrammer + 1), 'Rp' . number_format($item['bonus'], 2, ',', '.'), 1, 'C', false, 0, '', '', true, 0, false, true, 10 * ($jumlahTaskProgrammer + 1), 'M');
                    } else {                
                        $pdf->MultiCell(30, 10 * ($kuotaDataPerhalaman+1), 'Rp' . number_format($item['bonus'], 2, ',', '.'), 1, 'C', false, 0, '', '', true, 0, false, true, 10 * ($kuotaDataPerhalaman+1), 'M');
                    }                
                    // $pdf->MultiCell(30, 10 * ($kuotaDataPerhalaman+1), 'Rp' . number_format($item['bonus'], 2, ',', '.'), 1, 'L', false, 0, '', '', true, 0, false, true, 10 * ($kuotaDataPerhalaman+1), 'T');
                    $pdf->ln();
                } else {
                    // TASK
                    $pdf->MultiCell(75, 10, $val['name'], 1, 'L', 0, 0, $xTask, $yTask, true, 0, false, true, 10, 'M');
                    $jumlahTaskProgrammer--;
                    $kuotaDataPerhalaman--;
                    $yTask = $pdf->GetY() + 10;

                    // TIMELINE
                    foreach ($vals as $date2) {
                        if ($date2 == $row['date']) {
                            $pdf->MultiCell((75 / count($date['dates'])), 10, '', 1, 'C', 1, 0, '', '', true, 0, false, true, 10, 'M');
                        } else {
                            $pdf->MultiCell((75 / count($date['dates'])), 10, '', 1, 'C', 0, 0, '', '', true, 0, false, true, 10, 'M');
                        }
                    }

                    // RATE AND NOMINAL 
                    if ($differentDate) {
                        if ($row['jumlah'] > ($kuotaDataPerhalaman+1)) {
                            $pdf->MultiCell(30, (10 * ($kuotaDataPerhalaman+1)), 'Rp'. number_format($row['rate'], 2, ',', '.'), 1, 'C', 0, 0, $xRate, $yRate, true, 0, false, true, 10 * ($kuotaDataPerhalaman+1), 'M');
                            $pdf->MultiCell(30, (10 * ($kuotaDataPerhalaman+1)), 'Rp '. number_format($row['nominal'], 2, ',', '.'), 1, 'C', 0, 0, $xRate+40, $yRate, true, 0, false, true, 10 * ($kuotaDataPerhalaman+1), 'M');
                            // $yRate = $pdf->GetY() + (10 * $row['jumlah']);
                            $utangRowspan = $row['jumlah'] - ($kuotaDataPerhalaman+1);
                        } else {
                            $pdf->MultiCell(30, (10 * $row['jumlah']), 'Rp'. number_format($row['rate'], 2, ',', '.'), 1, 'C', 0, 0, $xRate, $yRate, true, 0, false, true, (10 * $row['jumlah']), 'M');
                            $pdf->MultiCell(30, (10 * $row['jumlah']), 'Rp '. number_format($row['nominal'], 2, ',', '.'), 1, 'C', 0, 0, $xRate+40, $yRate, true, 0, false, true, (10 * $row['jumlah']), 'M');
                            $yRate = $pdf->GetY() + (10 * $row['jumlah']);
                        }
                    }

                    // PROGRESS
                    $pdf->MultiCell(10, 10, $val['progress']. '%', 1, 'C', 0, 0, $xRate+30, $yProgress, true, 0, false, true, 10, 'M');
                    $yProgress = $pdf->GetY() + 10;

                    // BONUS
                    $pdf->Ln();
                }
                $differentProgrammer = false;
                $differentDate = false;
                if ($countData != 0 && $countData%19 == 0 && $halaman != 1) {
                    $kuotaDataPerhalaman = 19;
                    $yProgress = '';
                    $yRate = '';
                    $yTask = '';
                    $pdf->AddPage();
                    $halaman++;
                    if ($jumlahTaskProgrammer > 0) {
                        $pdf->MultiCell(30, 10 * $jumlahTaskProgrammer, "", 1, 'L', 0, 0, 10, '', true, 0, false, true, 10 * $jumlahTaskProgrammer, 'M');
                        $pdf->MultiCell(30, 10 * $jumlahTaskProgrammer, '', 1, 'C', false, 0, $xNominal, '', true, 0, false, true, 10 * $jumlahTaskProgrammer, 'M');
                    }
                    if ($utangRowspan != 0) {
                        $pdf->MultiCell(30, (10 * $utangRowspan), 'Rp'. number_format($row['rate'], 2, ',', '.'), 1, 'C', 0, 0, $xRate, $yRate, true, 0, false, true, 10 * $utangRowspan, 'M');
                        $pdf->MultiCell(30, (10 * $utangRowspan), 'Rp '. number_format($row['nominal'], 2, ',', '.'), 1, 'C', 0, 0, $xRate+40, $yRate, true, 0, false, true, 10 * $utangRowspan, 'M');
                        $utangRowspan = 0;
                    }
                    // $pdf->MultiCell(30, 10 * 14, "", 1, 'L', 0, 0, '', '', true, 0, false, true, 0, 'T');
                    // $pdf->MultiCell(30, 10 * 14, "", 1, 'L', 0, 0, $xRate, '', true, 0, false, true, 0, 'T');
                } else if ($countData != 0 && $countData%14 == 0 && $halaman == 1) {
                    $countData = 0;
                    $kuotaDataPerhalaman = 19;
                    $yProgress = '';
                    $yRate = '';
                    $yTask = '';
                    $pdf->AddPage();
                    if ($jumlahTaskProgrammer > 0) {
                        $pdf->MultiCell(30, 10 * $jumlahTaskProgrammer, '', 1, 'L', 0, 0, 10, '', true, 0, false, true, 10 * $jumlahTaskProgrammer, 'M');
                        $pdf->MultiCell(30, 10 * $jumlahTaskProgrammer, '', 1, 'C', false, 0, $xNominal, '', true, 0, false, true, 10 * $jumlahTaskProgrammer, 'M');
                    }
                    if ($utangRowspan != 0) {
                        $pdf->MultiCell(30, (10 * $utangRowspan), 'Rp'. number_format($row['rate'], 2, ',', '.'), 1, 'C', 0, 0, $xRate, $yRate, true, 0, false, true, 10 * $utangRowspan, 'M');
                        $pdf->MultiCell(30, (10 * $utangRowspan), 'Rp '. number_format($row['nominal'], 2, ',', '.'), 1, 'C', 0, 0, $xRate+40, $yRate, true, 0, false, true, 10 * $utangRowspan, 'M');
                        $utangRowspan = 0;
                    }
                    $halaman++;
                    // $pdf->MultiCell(30, 10 * 14, "", 1, 'L', 0, 0, '', '', true, 0, false, true, 0, 'T');
                }
            }
        }
        // $pdf->Ln();
    }
    
    // $pdf->writeHTML($html, true);
    // output
    $pdf->Output($project->sprint . '.pdf', 'I');
    exit;
});