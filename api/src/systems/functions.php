<?php
use Service\Landa;
use Service\Db;

function arr_date_to_string($date)
{
    $combine_date = $date['day'].'-'.$date['month'].'-'.$date['year'];

    return date('Y-m-d', strtotime($combine_date));
}

function twigView()
{
    return new \Slim\Views\Twig('src/views');
}
function generate_kodegaji($tahun, $bulan)
{
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);
    $cekKode = $db->select('kode')
        ->from('t_payroll')
        ->orderBy('kode DESC')
        ->find()
    ;
    if ($cekKode) {
        $kode_terakhir = $cekKode->kode;
    } else {
        $kode_terakhir = 0;
    }

    $tipe = 'PYR';
    $kode_item = (substr($kode_terakhir, -4) + 1);
    $kode = substr('0000'.$kode_item, strlen($kode_item));

    return $tipe.$tahun.$bulan.$kode;
}
function sendMailreg($subjek, $nama_penerima, $email_penerima, $template)
{
    $body = $template;
    // $db   = new Cahkampung\Landadb(Db());
    $config = config('DB');
    $db = new Cahkampung\Landadb($config['db']);
    $mail = new PHPMailer\PHPMailer\PHPMailer();
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->SMTPOptions = [
        'ssl' => [
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true,
        ],
    ];
    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
//    $mail->Username = "erliomedia@gmail.com";
    // $mail->Password = "bismillah17";
    // $mail->Username = "noreplyinfosystems@gmail.com";
    // $mail->Password = "bismillah2018";
    $mail->Username = 'giromarutori@gmail.com';
    $mail->Password = 'bvojyztqsdpvzpie';
//        $mail->Username = $getEmail->email_smtp;
    //        $mail->Password = $getEmail->password_smtp;
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;
    $mail->setFrom('noreplyinfosystems@gmail.com', 'HUMANIS APP');
    $mail->addAddress($email_penerima, "{$nama_penerima}");
    $mail->isHTML(true);
    $mail->Subject = $subjek;
    $mail->Body = $body;
    // if ($file != false) {
    //     $mail->AddAttachment($file, "laporan-data-nup.pdf");
    // }
    if (!$mail->send()) {
        return [
            'status' => false,
            'error' => $mail->ErrorInfo,
        ];
    }

    return [
        'status' => true,
    ];
}

// Function Hari laporan rkehadiran
function tambahhari($tahun, $bulan, $tanggal)
{
    $tanggalnye = $tahun.'-'.$bulan.'-'.$tanggal;
    $tanggalnye_format = date('Y-m-d', strtotime($tanggalnye));

    return date('l', strtotime($tanggalnye_format));
}
// Function Print_r Die
function pd($params)
{
    print_r($params);

    exit;
}

// Function Echo JSON ENCODE DIE
function ej($params)
{
    echo json_encode($params);

    exit;
}


// generator kode foto


function generate_kode_file()
{
    $dbModel = Db::db();
    $cekKode = $dbModel->select('id')
        ->from('m_petani_foto')
        ->orderBy('id DESC')
        ->find()
    ;

    try {
        if ($cekKode) {
            $kode_terakhir = $cekKode->id;
        } else {
            $kode_terakhir = 0;
        }
        $tipe = 'PT';
        $kode_item = (substr($kode_terakhir, -4) + 1);
        $kode = substr('0000'.$kode_item, strlen($kode_item));
        $kode = $tipe.$kode;

        return [
            'status' => true,
            'data' => $kode,
        ];
    } catch (Exception $e) {
        return [
            'status' => false,
            'error' => 'Gagal Generate Kode',
        ];
    }
}

// generat kode untuk tambak

function generate_kode_foto_tambak()
{
    $dbModel = Db::db();
    $cekKode = $dbModel->select('id')
        ->from('m_tambak_foto_dokumen')
        ->orderBy('id DESC')
        ->find()
    ;

    try {
        if ($cekKode) {
            $kode_terakhir = $cekKode->id;
        } else {
            $kode_terakhir = 0;
        }
        $tipe = 'PT';
        $kode_item = (substr($kode_terakhir, -4) + 1);
        $kode = substr('0000'.$kode_item, strlen($kode_item));
        $kode = $tipe.$kode;

        return [
            'status' => true,
            'data' => $kode,
        ];
    } catch (Exception $e) {
        return [
            'status' => false,
            'error' => 'Gagal Generate Kode',
        ];
    }
}



// function save foto banyak petani

function saveFilePetani($params)
{
    $landa = new Landa();
    if (isset($params['base64']) && !empty($params['base64'])) {
        $path = 'assets/filePetani/';
        $kode =  generate_kode_file();
        $batas = strpos($params['base64'], 'base64,');
        $batas_potong = $batas + 7;
        $file['filename'] = $params['filename'];
        $file['base64'] = substr($params['base64'], $batas_potong);
        $uploadFile = $landa->base64ToFilePath($file, $path, $kode['data']);
        $customnamafile = $uploadFile['data']['fileName'];
        if ($uploadFile['status']) {
            return  $customnamafile;
        }

        return unprocessResponse('gagal', [$uploadFile['error']]);
    }
}

function saveFilePandega($params)
{
    $landa = new Landa();
    if (isset($params['base64']) && !empty($params['base64'])) {
        $path = 'assets/filePandega/';
        $kode =  generate_kode_file();
        $batas = strpos($params['base64'], 'base64,');
        $batas_potong = $batas + 7;
        $file['filename'] = $params['filename'];
        $file['base64'] = substr($params['base64'], $batas_potong);
        $uploadFile = $landa->base64ToFilePath($file, $path, $kode['data']);
        $customnamafile = $uploadFile['data']['fileName'];
        if ($uploadFile['status']) {
            return  $customnamafile;
        }

        return unprocessResponse('gagal', [$uploadFile['error']]);
    }
}



// function save foto banyak tambak

function saveFileTambak($params)
{
    $landa = new Landa();
    if (isset($params['base64']) && !empty($params['base64'])) {
        $path = 'assets/fileDokumenTambak/';
        $kode = generate_kode_foto_tambak();
        $batas = strpos($params['base64'], 'base64,');
        $batas_potong = $batas + 7;
        $file['filename'] = $params['filename'];
        $file['base64'] = substr($params['base64'], $batas_potong);
        $uploadFile = $landa->base64ToFilePath($file, $path, $kode['data']);
        $customnamafile = $uploadFile['data']['fileName'];
        if ($uploadFile['status']) {
            return  $customnamafile;
        }

        return unprocessResponse('gagal', [$uploadFile['error']]);
    }
}
function saveFileTambakLahan($params)
{
    $landa = new Landa();
    if (isset($params['base64']) && !empty($params['base64'])) {
        $path = 'assets/fileDokumenLahan/';
        $kode = generate_kode_foto_tambak();
        $batas = strpos($params['base64'], 'base64,');
        $batas_potong = $batas + 7;
        $file['filename'] = $params['filename'];
        $file['base64'] = substr($params['base64'], $batas_potong);
        $uploadFile = $landa->base64ToFilePath($file, $path, $kode['data']);
        $customnamafile = $uploadFile['data']['fileName'];
        if ($uploadFile['status']) {
            return  $customnamafile;
        }

        return unprocessResponse('gagal', [$uploadFile['error']]);
    }
}
function saveFile($params, $path = 'assets/filePengajuanIzin/', $kode = 'F')
    {
        return $params;
        // $landa = new Landa();
        // if (isset($params) && !empty($params)) {
        //      if (!file_exists($path)) {
        //         mkdir($path, 0777, true);
        //     }           
        //     $file['filename'] = isset($params['filename']) ? $params['filename'] : time() . '.jpg';
        //     $file['base64'] = $params;
        //     $uploadFile = $landa->base64ToFilePath($file, $path, $kode);
        //     $customnamafile = $uploadFile['data']['fileName'];
        //     if ($uploadFile['status']) {
        //         return $customnamafile;
        //     }

        //     return unprocessResponse('gagal', [$uploadFile['error']]);
        // }
    }

//  get kota 

 function getDataKota($id)
{
    $dbModel = Db::db();
    $dbModel->select('*')
        ->from('wilayah_kabupaten')->where('is_deleted', '=', '0');

    if ('0' != $id) {
        $dbModel->andWhere('provinsi_id', '=', $id);
    }

    $models = $dbModel->findAll();
    $totalItem = $dbModel->count();

    return [
        'data' => $models,
        'totalItem' => $totalItem,
    ];
}

// generate kode petani
 function generate_kode_petani()
{
    $dbModel = Db::db();
    $cekKode = $dbModel->select('id')
        ->from('m_petani')
        ->orderBy('id DESC')
        ->find()
    ;

    try {
        if ($cekKode) {
            $kode_terakhir = $cekKode->id;
        } else {
            $kode_terakhir = 0;
        }
        $tahun = date('y');
        $bulan = date('m');
        $tipe = 'PET';
        $kode_item = (substr($kode_terakhir, -5) + 1);
        $kode = substr('00000'.$kode_item, strlen($kode_item));
        $kode = $tipe.$tahun.$bulan.$kode;

        return [
            'status' => true,
            'data' => $kode,
        ];
    } catch (Exception $e) {
        return [
            'status' => false,
            'error' => 'Gagal Generate Kode',
        ];
    }
}


// generate kode tambak
function generate_kode_tambak()
{
    $dbModel = Db::db();
    $cekKode = $dbModel->select('id')
        ->from('m_tambak')
        ->orderBy('id DESC')
        ->find()
    ;

    try {
        if ($cekKode) {
            $kode_terakhir = $cekKode->id;
        } else {
            $kode_terakhir = 0;
        }
        $tahun = date('y');
        $bulan = date('m');
        $tipe = 'TMBK';
        $kode_item = (substr($kode_terakhir, -5) + 1);
        $kode = substr('00000'.$kode_item, strlen($kode_item));
        $kode = $tipe.$tahun.$bulan.$kode;

        return [
            'status' => true,
            'data' => $kode,
        ];
    } catch (Exception $e) {
        return [
            'status' => false,
            'error' => 'Gagal Generate Kode',
        ];
    }
}

function status_bahan_penunjang($val)
{
  if (empty($val)) {
   return '-';
  }
  $val--;
  $array = ['direkomendasikan', 'diizinkan', 'dilarang'];
  $text = isset($array[$val]) ? $array[$val] : '-';
  return $text;
}

function getIdByCode($table, $code, $where = 'kode_form')
{
    $db = Db::db();
    
    $data = $db->select('id')
    ->from($table)
    ->where($where, '=', $code)
    ->find();

    return isset($data->id) ? $data->id : 0;
}

function getSizeId($size, $penerimaan_id)
{
  $db = Db::db();
  $size = is_numeric($size) ? round($size) : $size;
  $data = $db->select('m_size_id')->from('t_penerimaan_det')
  ->innerJoin('m_size', 'm_size.id = t_penerimaan_det.m_size_id')
  ->where('t_penerimaan_id', '=', $penerimaan_id)
  ->andWhere('m_size.nama', '=', $size)
  ->find();

  return isset($data->m_size_id) ? $data->m_size_id : 0;
}
function getGradeId($grade, $penerimaan_id)
{
  $db = Db::db();
  $grade = is_numeric($grade) ? round($grade) : $grade;
  $data = $db->select('m_grade_id')->from('t_penerimaan_det')
  ->innerJoin('m_grade', 'm_grade.id = t_penerimaan_det.m_grade_id')
  ->where('t_penerimaan_id', '=', $penerimaan_id)
  ->andWhere('m_grade.nama', '=', $grade)
  ->find();

  return isset($data->m_grade_id) ? $data->m_grade_id : 0;
}
function getSizeGlobal($size_id)
{
    $db = Db::db();

    $data = $db->select('m_size_global_id')
    ->from('m_size')
    ->where('id', '=', $size_id)
    ->find();

    return $data;
}
function generateHarga($grade, $size, $size_global = null)
{
    $db = Db::db();

    $db->select('m_harga_det.harga')
    ->from('m_harga_det')
    ->innerJoin('m_harga', 'm_harga.id = m_harga_det.m_harga_id');

    $db->andWhere('m_harga.m_grade_id', '=', $grade);

    if (!empty($size_global)) {
        $db->andWhere('m_harga_det.m_size_global_id', '=', $size_global);
    } else{
        $getSizeGlobal = getSizeGlobal($size);
        $size_gobal_id = isset($getSizeGlobal->m_size_global_id) ? $getSizeGlobal->m_size_global_id : 0;
        $db->andWhere('m_harga_det.m_size_global_id', '=', $size_gobal_id);
    }
    $db->orderBy('m_harga_det.id DESC');
    $db->limit(1);
    $data = $db->find();
   
    return isset($data->harga) ? $data->harga : 0;
}

function simpan_form($data, $tabel, $where = 'id', $value = null)
{
    $db = Db::db();
    if (!empty($data)) {
        if (!empty($value)) {
            $model = $db->update($tabel, $data, [$where => $value]);
        } else {
            $model = $db->insert($tabel, $data);
        } 
    } 

    return isset($model) ? $model : [];
}

function simpan_form_multiWhere($data, $tabel, $where)
{
    $db = Db::db();
    $db->select('id')->from($tabel);

    if (!empty($where)) {
        foreach ($where as $k => $v) {
            $db->andWhere($k, '=', $v);
        }
    }
    $model = $db->find();

    if (isset($model->id)) {
        $model = $db->update($tabel, $data, ['id' => $model->id]);
    } else {
        $model = $db->insert($tabel, $data);
    } 

    return isset($model) ? $model : [];
}

function simpan_form_det($data, $tabel, $where, $value = null, $tgl = null, $valTipe = null, $tipe = 'tipe')
{
    $db = Db::db();

    if (!empty($data)) {
        if (!empty($valTipe)) {
            $db->delete($tabel, [$where => $value, $tipe => $valTipe]);
        } else {
            $db->delete($tabel, [$where => $value]);
        }
        foreach ($data as $k => $v) {
            if (isset($v['id'])) {
                unset($v['id']);
            }
            $v[$where]          = $value;

            if (!empty($valTipe)) {
                $v[$tipe] = $valTipe;
            }

            if (!empty($tgl)) {
                foreach ($tgl as $ke => $val) {
                    $v[$val] = isset($v[$val]) ? date('Y-m-d H:i:s', strtotime($v[$val])) : null;
                }
            }
            $model = $db->insert($tabel, $v);
        }
    }

    return isset($model) ? true : false;
}

function arrayToString($array, $index = null, $sparator = ',')
{
    $data = [0];
    if (!empty($array)) {
        $data = [];
        foreach ($array as $key => $val) {
            if (!empty($index)) {
                $data[] = isset($val[$index]) ? $val[$index] : '';
            } else{
                $data[] = $val;
            }
        }
    }
    $data = implode($sparator, $data);
    
    return $data;
}

function stringToArray($string, $index = null, $sparator = ','){
    $data = [];
    if (!empty($string)) {
        $string = str_replace(' ', '', $string);
        $arr = explode($sparator, $string);

        if (!empty($arr)) {
            foreach ($arr as $key => $val) {
               if (!empty($index)) {
                   $data[$key][$index] = $val;
               } else{
                   $data[] = $val;
               }
            }
        }
    }

    return $data;
}

function get_form_induk($tabel, $param, $verif = null, $params = null, $limit = 0)
{
    $db = Db::db();

    $db->select('*')
    ->from($tabel); 
    if (!empty($params)) {
        if (isset($params['limit'])) {
            $limit = $params['limit'];

            unset($params['limit']);
        }
        $i = 0;
        foreach ($params as $ke => $val) {
            if ($i == 0) {
                $db->customWhere($ke .' IN ('. $val . ')');
            } else{
                 $db->customWhere($ke .' IN ('. $val . ')', "AND");
             }
            $i++;
        }
    }
    if (!empty($limit)) {

       $db->limit($limit);
    }
    $db->orderBy('id DESC');
    $data = $db->findAll();
    $dataInduk = [];
    if (!empty($data)) {
    $data = json_decode(json_encode($data), true);
        foreach ($data as $ke => $val) {
            foreach ($param as $vals) {
                $dataInduk[$ke][$vals] = isset($val[$vals]) ? $val[$vals] : null;
            }

            $arrVerif = [];
            if (!empty($verif)) {
                foreach ($verif as $v) {
                   $arrVerif[$v] = isset($val[$v]) ? $val[$v] : null;
                }
                $dataInduk[$ke]['data_verifikasi'] = $arrVerif;
            }
        }
    }

    return !empty($dataInduk) ? $dataInduk : null;    
}

function get_form_data($tabel, $whereId ,$id_induk, $where = null)
{
    $db = Db::db();

    $db->select('*')
    ->from($tabel);

    $db->customWhere( $whereId . ' IN ('. $id_induk. ')');
    // $db->andWhere('is_deleted', '=', 0);

    if (!empty($where)) {
       foreach ($where as $k => $v) {
           $db->andWhere($k, '=', $v);
       }
    }
    $data = $db->findAll();
    // ej($data);
    $data = json_decode(json_encode($data), true);
    $dataForm = [];
    if (!empty($data)) {
        foreach ($data as $key => $value) {
            $dataForm[$value[$whereId]] = $value;
        }
    }
    
    return !empty($dataForm) ? $dataForm : null;
}

function get_form_detail($tabel, $whereId ,$id_data, $where = null)
{
    $db = Db::db();

    $db->select('*')
    ->from($tabel);

    $db->customWhere($whereId . ' IN ('. $id_data. ')');

    if (!empty($where)) {
       foreach ($where as $k => $v) {
           $db->andWhere($k, '=', $v);
       }
    }
    $data = $db->findAll();

    $data = json_decode(json_encode($data), true);
    $dataForm = [];
    if (!empty($data)) {
        foreach ($data as $key => $value) {
            $dataForm[$value[$whereId]][] = $value;
        }
    }
    
    return !empty($dataForm) ? $dataForm : null;
}
function get_detail_from_data($induk, $where, $default = null, $lv = 0, $index = 0)
{
    $data = [];
    if (!empty($induk)) {
        foreach ($induk as $key => $val) {
            if (!empty($where)) {
                foreach ($where as $v) {
                    if (!empty($lv)) {                        
                        $data[$val['id']][$index][$v] = isset($val[$v]) ? $val[$v] : $default;
                    } else{
                        $data[$val['id']][$v] = isset($val[$v]) ? $val[$v] : $default;
                    }
                }
            }
        }
    }

    return !empty($data) ? $data : null;
}

function gabungkanArrayData($induk, $index, $array = null)
{
    $data = [];
    if (!empty($induk)) {
        foreach ($induk as $key => $value) {
            $data[$value[$index]]['form'] = $value;

            if (!empty($array)) {
                foreach ($array as $k => $val) {
                    $data[$value[$index]][$k] = isset($val[$value['id']]) ? $val[$value['id']] : null;
                }
            }
        }
    }

    return !empty($data) ? $data : null;
}
function gabungkanArrayInduk($induk, $array = null)
{
    $data = [];
    if (!empty($induk)) {
        foreach ($induk as $key => $value) {
            $data[$key] = $value;

            if (!empty($array)) {
                foreach ($array as $k => $val) {
                    $data[$key][$k] = isset($val[$value['id']]) ? $val[$value['id']] : null;
                }
            } 
        }
    }

    return !empty($data) ? $data : null;
}

function simpan_approval($reff_type, $alias, $params, $is_approve = 0, $id = 0, $reff_id = null, $jenis = 'laporan')
{
    $db = Db::db();

    if (isset($params['is_approve'])) {
        unset($params['is_approve']);
    }

    if (isset($params['id'])) {
        unset($params['id']);
    }

    $data = [
        'reff_type'     => $reff_type,
        'path'          => $reff_type,
        'alias'         => $alias,
        'is_approve'    => !empty($is_approve) && $is_approve > 1 ? 1 : 0,
        'jenis'         => $jenis,
        'reff_id'       => $reff_id,
        'filter'        => json_encode($params)
    ];

    if (!empty($is_approve) && $is_approve > 1) {
        $data['approved_at'] = date('Y-m-d H:i:s');
        $data['approved_by'] = isset($_SESSION['user']['id']) ? $_SESSION['user']['id'] : 0;
    }

    if (!empty($id)) {
       $model = $db->update('t_audit_approval', $data, ['id' => $id]);
    } else {
       $model = $db->insert('t_audit_approval', $data);
    }

    return $model;
}

function get_audit_appoval($param = [], $reff_type = null, $is_approve = 0, $jenis = null)
{
   $db = Db::db();
   $db->select('*')
    ->from('t_audit_approval');

    if (!empty($param)) {

        foreach ($param as $key => $val) {
            $k = '$.' . $key;
            if ($key != 'tanggal_mulai' && $key != 'tanggal_selesai' && $key != 'tahun_selesai' && $key != 'tahun_selesai' && $key != 'id') {
                $db->customWhere('JSON_EXTRACT(filter, "' .$k. '") = "' . $val. '"', "AND");
            }
        }

        /*=========================== filter tanggal =================================*/

        $tgl_mulai = '';
        if (isset($param['tanggal_mulai']) && !empty($param['tanggal_mulai'])) {
            $tgl_mulai = date('Y-m-d', strtotime($param['tanggal_mulai']));

        }

        $tgl_selesai = '';
        if (isset($param['tanggal_selesai']) && !empty($param['tanggal_selesai'])) {
            $tgl_selesai = date('Y-m-d', strtotime($param['tanggal_selesai']));

        }

        if (!empty($tgl_mulai) && !empty($tgl_selesai)) {
            $db->customWhere('JSON_EXTRACT(filter, "$.tanggal_mulai") <= "' . $tgl_selesai. '" AND JSON_EXTRACT(filter, "$.tanggal_selesai") >= "' . $tgl_mulai. '"', "AND");
        } elseif (!empty($tgl_mulai)) {
            $db->customWhere('JSON_EXTRACT(filter, "$.tanggal_mulai") = "' . $tgl_mulai. '"', "AND");
        } elseif (!empty($tgl_selesai)) {
            $db->customWhere('JSON_EXTRACT(filter, "$.tanggal_selesai") = "' . $tgl_selesai. '"', "AND");
        }
        /*=========================== end filter tanggal =================================*/

        /*=========================== filter tahun =================================*/

        $tahun_mulai = '';
        if (isset($param['tahun_mulai']) && !empty($param['tahun_mulai'])) {
            $tahun_mulai = $param['tahun_mulai'];
        }

        $tahun_selesai = '';
        if (isset($param['tahun_selesai']) && !empty($param['tahun_selesai'])) {
            $tahun_selesai = $param['tahun_selesai'];
        }

        if (!empty($tahun_mulai) && !empty($tahun_selesai)) {
            $db->customWhere('JSON_EXTRACT(filter, "$.tahun_mulai") <= "' . $tahun_selesai. '" AND JSON_EXTRACT(filter, "$.tahun_selesai") >= "' . $tahun_mulai. '"', "AND");
        } elseif (!empty($tahun_mulai)) {
            $db->customWhere('JSON_EXTRACT(filter, "$.tahun_mulai") = "' . $tahun_mulai. '"', "AND");
        } elseif (!empty($tahun_selesai)) {
            $db->customWhere('JSON_EXTRACT(filter, "$.tahun_selesai") = "' . $tahun_selesai. '"', "AND");
        }

        /*=========================== end filter tahun =================================*/
    }
    if (!empty($is_approve)) {
        $db->andWhere('is_approve', '=', $is_approve);
    }
    if (!empty($reff_type)) {
        $db->andWhere('reff_type', '=', $reff_type);
    }
    if (!empty($jenis)) {
        $db->andWhere('jenis', '=', $jenis);
    }
   $data = $db->findAll();

   $data = !empty($data) ? json_decode(json_encode($data), true) : [];

   return $data;
}
function createHead($data)
{
    $arr = [];
    if (!empty($data)) {
        foreach ($data as $key => $val) {
            foreach ($val as $k => $v) {
                $arr[$k] = ucwords(str_replace('_', ' ', $k));
            }
        }
    }

    return $arr;
}

function getArrayF6($arr, $where, $idx)
{
    $data = [];
    if (!empty($arr)) {
        foreach ($arr as $key => $val) {
           $row = isset($val[$where]) && !empty($val[$where]) ? stringToArray($val[$where], $idx) : null;
           $data[$val['id']] = $row;
        }
    }

    return $data;
}