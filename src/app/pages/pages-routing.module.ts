import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../core/guards/auth.guard';


const routes: Routes = [
  { path: '', redirectTo: 'project' },
  // { path: 'home', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'project', loadChildren: () => import('./project/project.module').then(m => m.ProjectModule), canActivate: [] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
