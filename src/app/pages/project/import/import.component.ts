import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { LandaService } from '../../../core/services/landa.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.scss']
})
export class ImportComponent implements OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtInstance: Promise<DataTables.Api>;
  dtOptions: any;

  breadCrumbItems: Array<{}>;

  pageTitle: string;
  isView: boolean;
  isEdit: boolean;
  listData: any;
  showForm: boolean;

  model: {
    id,
    kode,
    nama
  };
  modelParam: {
    nama,
    kode
  };
  kode: string;

  constructor(private landaService: LandaService, private router: Router) { }

  ngOnInit(): void {
    this.pageTitle = 'Import aja';
    this.breadCrumbItems = [
      {
        label: 'Project'
      },
      {
        label: 'Import',
        active: true,
      }
    ];
    this.modelParam = {
      nama: '',
      kode: '',
    };
    this.getData();
    this.empty();
  }

  pencarian(): void {
    this.reloadDataTable();
  }

  reloadDataTable(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }

  getData() {
    this.dtOptions = {
      serverSide: true,
      processing: true,
      ordering: false,
      pagingType: 'full_numbers',
      ajax: (dataTablesParameters: any, callback) => {
        const params = {
          filter: JSON.stringify(this.modelParam),
          offset: dataTablesParameters.start,
          limit: dataTablesParameters.length,
        };
        this.landaService
          .DataGet('/coba/index', params)
          .subscribe((res: any) => {
            this.listData = res.data.list;
            this.kode = res.data.kode;
            callback({
              recordsTotal: res.data.totalItems,
              recordsFiltered: res.data.totalItems,
              data: [],
            });
          });
      },
    };
  }

  empty() {
    this.model = {
      id: null,
      nama: '',
      kode: ''
    };
    this.getData();
  }

  index() {
    this.showForm = !this.showForm;
    this.pageTitle = 'Data coba';
    this.getData();
    this.isEdit = false;
  }

  create() {
    this.empty();
    this.showForm = !this.showForm;
    this.pageTitle = 'Tambah Data Coba';
    this.model.kode = this.kode;
    this.isView = false;
  }

  edit(val) {
    this.showForm = !this.showForm;
    this.model = val;
    this.pageTitle = 'Coba : ' + val.nama;
    this.getData();
    this.isView = false;
    this.isEdit = true;
  }

  view(val) {
    this.showForm = !this.showForm;
    this.model = val;
    this.pageTitle = 'Coba : ' + val.nama;
    this.getData();
    this.isView = true;
  }

  save() {
    this.landaService.DataPost('/coba/save', this.model).subscribe((res: any) => {
      if (res.status_code === 200) {
        this.landaService.alertSuccess('Berhasil', 'Data Coba telah disimpan!');
        this.index();

      } else {
        this.landaService.alertError('Mohon Maaf', res.errors);
      }
    });
  }

  delete(val) {
    const data = {
      id: val != null ? val.id : null,
    };
    Swal.fire({
      title: 'Apakah anda yakin ?',
      text: 'Menghapus data perusahaan akan berpengaruh terhadap data lainnya',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Ya, Hapus data ini!'
    }).then(result => {
      if (result.value) {
        this.landaService.DataPost('/coba/hapus', data).subscribe((res: any) => {
          if (res.status_code === 200) {
            this.landaService.alertSuccess('Berhasil', 'Data Coba telah dihapus !');
            this.reloadDataTable();
          } else {
            this.landaService.alertError('Mohon Maaf', res.errors);
          }
        });
      }
    });
  }

}
