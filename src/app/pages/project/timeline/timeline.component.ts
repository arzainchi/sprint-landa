import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { LandaService } from '../../../core/services/landa.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtInstance: Promise<DataTables.Api>;
  dtOptions: any;

  breadCrumbItems: Array<{}>;

  pageTitle: string;
  isView: boolean;
  isEdit: boolean;
  listData: any;
  showForm: boolean;

  apiURL = environment.apiURL;
  model: {
    id,
    name,
    period,
    sprint,
  };
  modelParam: {
    name,
  };
  /****************************/
  /*** TIMELINE'S VARIABLES ***/
  /****************************/
  // -> header
  timeline: number;
  months: Array<{
    name,
    colspan
  }>;
  dates: Array<any>;

  // -> body
  fullDates: any;
  differentDate: boolean;
  differentProgrammer: boolean;
  dataTimeline: any;
  dataExcel: any;
  dataImport: any[];

  constructor(private landaService: LandaService, private router: Router) { }

  ngOnInit(): void {
    this.pageTitle = 'Timeline Project';
    this.breadCrumbItems = [
      {
        label: 'Project'
      },
      {
        label: 'Timeline',
        active: true,
      }
    ];
    this.modelParam = {
      name: '',
    };
    this.getData();
    this.empty();
  }

  pencarian(): void {
    this.reloadDataTable();
  }

  reloadDataTable(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }

  getData() {
    this.dtOptions = {
      serverSide: true,
      processing: true,
      ordering: false,
      pagingType: 'full_numbers',
      ajax: (dataTablesParameters: any, callback) => {
        const params = {
          filter: JSON.stringify(this.modelParam),
          offset: dataTablesParameters.start,
          limit: dataTablesParameters.length,
        };
        this.landaService
          .DataGet('/p_timeline/index', params)
          .subscribe((res: any) => {
            this.listData = res.data.list;
            callback({
              recordsTotal: res.data.totalItems,
              recordsFiltered: res.data.totalItems,
              data: [],
            });
          });
      },
    };
  }

  empty() {
    this.model = {
      id: null,
      name: '',
      period: '',
      sprint: ''
    };
    this.timeline = 0;
    this.differentDate = true;
    this.differentProgrammer = true;
    this.getData();
  }

  default() {
    this.showForm = !this.showForm;
    this.pageTitle = 'Timeline Project';
    this.dataImport = undefined;
    this.getData();
    this.isEdit = false;
  }

  create() {
    this.empty();
    this.showForm = !this.showForm;
    this.pageTitle = 'Tambah Timeline Project';
    this.isView = false;
  }

  edit(val) {
    this.showForm = !this.showForm;
    this.model = val;
    this.dataImport = val.timeline;
    this.setDateMonth(val.date);
    this.pageTitle = 'Timeline Project : ' + val.name;
    this.getData();
    this.isView = false;
    this.isEdit = true;
  }

  view(val) {
    this.showForm = !this.showForm;
    this.model = val;
    this.dataImport = val.timeline;
    this.setDateMonth(val.date);
    this.pageTitle = 'Timeline Project : ' + val.nama;
    this.getData();
    this.isView = true;
  }

  save() {
    const form = {
      dataProject: this.model,
      dataTimeline: this.dataImport,
    };
    this.landaService.DataPost('/p_timeline/save', form).subscribe((res: any) => {
      if (res.status_code === 200) {
        this.landaService.alertSuccess('Berhasil', 'Data Project telah disimpan!');
        this.default();
      } else {
        this.landaService.alertError('Mohon Maaf', res.errors);
      }
    });
  }

  delete(val) {
    const data = {
      id: val != null ? val.id : null,
    };
    Swal.fire({
      title: 'Apakah anda yakin ?',
      text: 'Menghapus data perusahaan akan berpengaruh terhadap data lainnya',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#f46a6a',
      confirmButtonText: 'Ya, Hapus data ini!'
    }).then(result => {
      if (result.value) {
        this.landaService.DataPost('/p_timeline/hapus', data).subscribe((res: any) => {
          if (res.status_code === 200) {
            this.landaService.alertSuccess('Berhasil', 'Data Timeline Project telah dihapus !');
            this.reloadDataTable();
          } else {
            this.landaService.alertError('Mohon Maaf', res.errors);
          }
        });
      }
    });
  }

  print(id) {
    window.open(this.apiURL + '/p_timeline/cetak?id=' + id, '_blank');
  }

  onFileSelected(event) {
    const target: DataTransfer = <DataTransfer> ( event.target );
    if (target.files.length === 1) {
      const reader: FileReader = new FileReader();
      reader.readAsBinaryString(target.files[0]);
      reader.onload = (e: any) => {
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});
        const wsname: string = wb.SheetNames[1];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];

        this.dataExcel = (XLSX.utils.sheet_to_json(ws, {header: 1}));
        this.dataExcel.shift(); // delete the header

        const rawData = this.dataExcel.slice(); // copy to rawData variable
        rawData.sort((a, b) => { // sort by due date
          if (a[10] < b[10]) {
            return -1;
          }
          if (a[10] > b[10]) {
            return 1;
          }
          return 0;
        });

        // Date and month to show in table
        const fullDate = [];
        const month = [];
        const date = [];
        const listMonth = [
          'Januari', 'Feburari', 'Maret', 'April', 'Mei', 'Juni',
          'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
        ];

        // get fullDate
        let index = 0;
        rawData.forEach(e => {
          if (fullDate.length === 0) {
            fullDate.push(e[10]);
          } else {
            if (fullDate.indexOf(e[10]) < 0) {
              fullDate.push(e[10]);
            }
          }
          index++;
        });
        this.fullDates = fullDate.slice();

        // get month and date
        index = 0;
        fullDate.forEach(e => {
          const d = new Date(e);
          date.push(d.getDate());
          if (month.length === 0) {
            const newValue = {
              name: listMonth[d.getMonth()],
              colspan: 1
            };
            month.push(newValue);
            this.timeline += 1;
            index++;
          } else {
            if (month.find(m => m.name === listMonth[d.getMonth()]) !== undefined) {
              month[(index - 1)].colspan += 1;
              this.timeline += 1;
            } else {
              const newValue = {
                name: listMonth[d.getMonth()],
                colspan: 1
              };
              month.push(newValue);
              this.timeline += 1;
              index++;
            }
          }
        });
        this.dates = date.slice();
        this.months = month.slice();

        // Mapping Data
        rawData.sort((a, b) => { // sort by asignee(programmer) then due date
          return a[3].localeCompare(b[3]) || b[10] - a[10];
        });
        const datae = [];
        let indexFullDate = 0;
        let indexTimeline = 0;
        index = 0;
        rawData.forEach(e => {
          if (datae.length === 0) {
            const row = {
              programmer: e[3],
              jumlah: 1,
              timeline: [
                {
                  date: fullDate[indexFullDate],
                  jumlah: 1,
                  tasks: [
                    {
                      name: e[2],
                      progress: 100,
                    },
                  ],
                  avgProgress: 100,
                  rate: 50000,
                  nominal: 0,
                },
              ],
              bonus: 0
            };
            datae.push(row);
            index++;
          } else {
            if (datae[(index - 1)].programmer === e[3]) { // jika programmernya sama
              if (datae[(index - 1)].timeline[indexTimeline].date === e[10]) { // jika tanggalnya sama
                const row = {
                  name: e[2],
                  progress: 100,
                };
                datae[(index - 1)].timeline[indexTimeline].jumlah += 1;
                datae[(index - 1)].timeline[indexTimeline].tasks.push(row);
              } else { // jika tanggalnya beda
                indexFullDate++;
                const row = {
                  date: fullDate[indexFullDate],
                  jumlah: 1,
                  tasks: [
                    {
                      name: e[2],
                      progress: 100,
                    },
                  ],
                  avgProgress: 100,
                  rate: 50000,
                  nominal: 0,
                };
                datae[(index - 1)].timeline.push(row);
                indexTimeline++;
              }
              datae[(index - 1)].jumlah += 1;
            } else { // jika programmernya beda
              indexFullDate = 0;
              indexTimeline = 0;
              const row = {
                programmer: e[3],
                jumlah: 1,
                timeline: [
                  {
                    date: fullDate[indexFullDate],
                    jumlah: 1,
                    tasks: [
                      {
                        name: e[2],
                        progress: 100,
                      },
                    ],
                    avgProgress: 100,
                    rate: 50000,
                    nominal: 0,
                  }
                ],
                bonus: 0
              };
              datae.push(row);
              index++;
            }
          }
        });
        this.dataImport = datae.slice(); // copy array
        index = 0;

        this.dataImport.forEach(e => {
          let indexTimeline = 0;

          e.timeline.forEach(e => {
            this.countNominal(index, indexTimeline);
            indexTimeline++;
          });

          this.countBonus(index);
          index++;
        });

        console.log(this.dataImport);
      };
    }
  }

  setDateMonth = (date) => {
    this.timeline = 0;
    const month = [];
    const datee = [];
    const listMonth = [
      'Januari', 'Feburari', 'Maret', 'April', 'Mei', 'Juni',
      'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
    ];

    let index = 0;
    date.forEach(e => {
      const d = new Date(e);
      datee.push(d.getDate());
      if (month.length === 0) {
        const newValue = {
          name: listMonth[d.getMonth()],
          colspan: 1
        };
        month.push(newValue);
        this.timeline += 1;
        index++;
      } else {
        if (month.find(m => m.name === listMonth[d.getMonth()]) !== undefined) {
          month[(index - 1)].colspan += 1;
          this.timeline += 1;
        } else {
          const newValue = {
            name: listMonth[d.getMonth()],
            colspan: 1
          };
          month.push(newValue);
          this.timeline += 1;
          index++;
        }
      }
    });
    this.fullDates = date.slice();
    this.dates = datee.slice();
    this.months = month.slice();
  }

  setDifferentProgrammer = (val: boolean) => {
    this.differentProgrammer = val;
  }

  setDifferentDate = (val: boolean) => {
    this.differentDate = val;
  }

  countBonus = (indexProgrammer) => {
    let bonus = 0;
    this.dataImport[indexProgrammer].timeline.forEach(e => {
      bonus += e.nominal;
    });
    this.dataImport[indexProgrammer].bonus = bonus;
  }

  countNominal = (indexProgrammer, index) => {
    const avg = this.avgProgress(indexProgrammer, index) / 100; // karna satuan persen(%)
    const rate = this.dataImport[indexProgrammer].timeline[index].rate;
    this.dataImport[indexProgrammer].timeline[index].avgProgress = avg;
    this.dataImport[indexProgrammer].timeline[index].nominal = rate * avg;
  }

  avgProgress = (indexProgrammer, index) => {
    let sum = 0;
    this.dataImport[indexProgrammer].timeline[index].tasks.forEach(e => {
      sum += e.progress;
    });
    return (sum / this.dataImport[indexProgrammer].timeline[index].jumlah);
  }
}
